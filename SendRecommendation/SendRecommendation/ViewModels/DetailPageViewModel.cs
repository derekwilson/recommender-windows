using SendRecommendation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Common;
using Template10.Mvvm;
using Template10.Services.NavigationService;
using Windows.UI.Xaml.Navigation;

namespace SendRecommendation.ViewModels
{
	public class DetailPageViewModel : ViewModelBase
	{
		public DetailPageViewModel()
		{
			if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
			{
				Value = "Designtime value";
			}
		}

		private ShareResult _Result;

		private string _Value = "Default";
		public string Value { get { return _Value; } set { Set(ref _Value, value); } }

		public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
		{
			if (suspensionState.ContainsKey(nameof(ShareResult)))
			{
				_Result = (ShareResult)suspensionState[nameof(ShareResult)];
				suspensionState.Clear();
			}
			else
			{
				_Result = (ShareResult)parameter;
			}
			if (_Result == null)
			{
				Value = "Unknown result";
			}
			else if (_Result.Error)
			{
				Value = _Result.ErrorMessage;
			}
			else
			{
				Value = "Share succeeded";
			}
			Views.Busy.SetBusy(false);
			await Task.CompletedTask;
		}

		public override async Task OnNavigatedFromAsync(IDictionary<string, object> suspensionState, bool suspending)
		{
			if (suspending)
			{
				suspensionState[nameof(ShareResult)] = _Result;
				suspensionState[nameof(Value)] = Value;
			}
			await Task.CompletedTask;
		}

		public override async Task OnNavigatingFromAsync(NavigatingEventArgs args)
		{
			args.Cancel = false;
			await Task.CompletedTask;
		}
	}
}

