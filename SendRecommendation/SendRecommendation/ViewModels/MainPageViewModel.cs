using Template10.Mvvm;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Template10.Services.NavigationService;
using Template10.Utils;
using Windows.UI.Xaml.Navigation;
using SendRecommendation.Models;
using SendRecommendation.Services.SharingServices;
using Windows.UI.Xaml;
using Windows.Devices.Enumeration;
using System.Collections.ObjectModel;
using Windows.System;
using MetroLog;

namespace SendRecommendation.ViewModels
{
	public class MainPageViewModel : ViewModelBase
	{
		private ILogger _logger;

		private Services.SettingsServices.SettingsService _settings;
		private BluetoothSharingService _blutoothService;

		/// <summary>
		/// This collection holds found BT devices.
		/// </summary>
		public ObservableCollection<DeviceInformation> BluetoothDevicesUi
		{
			get;
			private set;
		}

		public MainPageViewModel()
		{
			_logger = LogManagerFactory.DefaultLogManager.GetLogger<MainPageViewModel>();

			BluetoothDevicesUi = new ObservableCollection<DeviceInformation>();

			if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
			{
				Name = "Designtime title";
				Category = "Designtime category";
				Url = "Designtime url";
				EmailAddress = "Designtime emailaddress";
			}
			else
			{
				_settings = Services.SettingsServices.SettingsService.Instance;
			}

			_blutoothService = new BluetoothSharingService();
		}

		string _Name = "";
		public string Name { get { return _Name; } set { Set(ref _Name, value); } }

		string _Category = "";
		public string Category { get { return _Category; } set { Set(ref _Category, value); } }

		private String[] _Categories =
			{ "App", "Bar", "Book", "CD", "Film", "Massage", "Misc", "Music", "Podcast", "Restaurant", "Shop", "TV", "Talk", "Website", "Wine"};

		public string[] Categories
		{
			get
			{
				return _Categories;
			}
		}

		string _Url = "";
		public string Url { get { return _Url; } set { Set(ref _Url, value); } }

		string _Notes = "";
		public string Notes { get { return _Notes; } set { Set(ref _Notes, value); } }

		string _EmailAddress = "";
		public string EmailAddress { get { return _EmailAddress; } set { Set(ref _EmailAddress, value); } }

		private DeviceInformation _SelectedBluetoothDevice = null;
		public DeviceInformation SelectedBluetoothDevice
		{
			get
			{
				return _SelectedBluetoothDevice;
			}
			set
			{
				Set(ref _SelectedBluetoothDevice, value);
				UpdateBluetoothEnabled();
			}
		}

		private bool _BluetoothEnabled;
		public bool BluetoothEnabled
		{
			get { return _BluetoothEnabled; }
			set { Set(ref _BluetoothEnabled, value); }
		}

		protected void UpdateBluetoothEnabled()
		{
			BluetoothEnabled = (BluetoothDevicesUi.Count > 0 && SelectedBluetoothDevice != null);
		}

		private string _BluetoothVisible = "Visible";
		public string BluetoothVisible
		{
			get { return _BluetoothVisible; }
			set { Set(ref _BluetoothVisible, value); }
		}

		protected void UpdateBluetoothVisible()
		{
			BluetoothVisible = BluetoothDevicesUi.Count > 0 ? "Visible" : "Collapsed";
		}

		public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
		{
			if (_settings != null)
			{
				(Window.Current.Content as FrameworkElement).RequestedTheme = _settings.AppTheme.ToElementTheme();
				if (string.IsNullOrWhiteSpace(EmailAddress))
				{
					// dont overwrite if the user has filled it in
					EmailAddress = _settings.EmailAddress;
				}
			}

			string suspendedBluetoothId = null;
			if (suspensionState.Any())
			{
				Name = suspensionState[nameof(Name)]?.ToString();
				Url = suspensionState[nameof(Url)]?.ToString();
				Notes = suspensionState[nameof(Notes)]?.ToString();
				EmailAddress = suspensionState[nameof(EmailAddress)]?.ToString();
				suspendedBluetoothId = suspensionState[nameof(SelectedBluetoothDevice)]?.ToString();
			}

			if (BluetoothDevicesUi.Count < 1)
			{
				// get the bluetooth pairs
				EnumeratePairedDevices(suspendedBluetoothId);
			}
			await Task.CompletedTask;
		}

		public override async Task OnNavigatedFromAsync(IDictionary<string, object> suspensionState, bool suspending)
		{
			if (suspending)
			{
				suspensionState[nameof(Name)] = Name;
				suspensionState[nameof(Url)] = Url;
				suspensionState[nameof(Notes)] = Notes;
				suspensionState[nameof(EmailAddress)] = EmailAddress;
				suspensionState[nameof(SelectedBluetoothDevice)] = SelectedBluetoothDevice.Id;
			}
			await Task.CompletedTask;
		}

		public override async Task OnNavigatingFromAsync(NavigatingEventArgs args)
		{
			args.Cancel = false;
			await Task.CompletedTask;
		}

		public async void SendEmail()
		{
			Views.Busy.SetBusy(true, "Sending...");
			Recommendation recommendation = new Recommendation()
			{
				Name = Name,
				Category = Category,
				Notes = Notes,
				Uri = Url
			};
			EmailSharingService service = new EmailSharingService();
			await service.Share(EmailAddress, recommendation, GotoDetailsPage);
			Views.Busy.SetBusy(false);
		}

		public async void SendBluetooth()
		{
			Views.Busy.SetBusy(true, "Sending...");
			Recommendation recommendation = new Recommendation()
			{
				Name = Name,
				Category = Category,
				Notes = Notes,
				Uri = Url
			};

			if (SelectedBluetoothDevice != null)
			{
				await _blutoothService.Share(SelectedBluetoothDevice, recommendation, GotoDetailsPage);
			}
			Views.Busy.SetBusy(false);
		}

		public async void LaunchBluetoothSettings()
		{
			await Launcher.LaunchUriAsync(new Uri("ms-settings-bluetooth:"));
		}

		public void Clear()
		{
			Name = "";
			Category = "";
			Url = "";
			Notes = "";
			EnumeratePairedDevices(null);
		}

		public async void CopyFromCLipboard()
		{
			ClipboardService service = new ClipboardService();
			Views.Busy.SetBusy(true, "Copying...");
			string text = await service.GetStringFromClipboard();
			if (text != null)
			{
				if (text.ToLower().StartsWith("http://") || text.ToLower().StartsWith("https://"))
				{
					Url = text;
				}
				else if (Name.Length > 0)
				{
					Notes = text;
				}
				else
				{
					Name = text;
				}
			}
			Views.Busy.SetBusy(false);
		}

		public async void EnumeratePairedDevices(string suspendedId)
		{
			BluetoothDevices devices = await _blutoothService.Init();
			_logger.Trace("Devices Paired: {0} {1}", devices.Error,  devices.Devices.Count);
			if (devices.Error == BluetoothDevices.BluetoothDeviceError.OK)
			{
				BluetoothDevicesUi.Clear();
				BluetoothDevicesUi.AddRange(devices.Devices);
				if (suspendedId != null)
				{
					SelectedBluetoothDevice = devices.Devices.FirstOrDefault(device => device.Id == suspendedId);
				}
				else if (BluetoothDevicesUi.Count > 0)
				{
					SelectedBluetoothDevice = BluetoothDevicesUi[0];
				}
				UpdateBluetoothVisible();
			}
		}

		public void GotoDetailsPage(ShareResult result) =>
			NavigationService.Navigate(typeof(Views.DetailPage), result);

		public void GotoSettings() =>
			NavigationService.Navigate(typeof(Views.SettingsPage), 0);

		public void GotoHelp() =>
			NavigationService.Navigate(typeof(Views.SettingsPage), 1);

		public void GotoTroubleshooting() =>
			NavigationService.Navigate(typeof(Views.SettingsPage), 2);

		public void GotoLicenses() =>
			NavigationService.Navigate(typeof(Views.SettingsPage), 3);

		public void GotoAbout() =>
			NavigationService.Navigate(typeof(Views.SettingsPage), 4);

	}
}

