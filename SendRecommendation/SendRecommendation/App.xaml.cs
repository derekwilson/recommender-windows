using System;
using Windows.UI.Xaml;
using System.Threading.Tasks;
using SendRecommendation.Services.SettingsServices;
using Windows.ApplicationModel.Activation;
using Template10.Mvvm;
using Template10.Common;
using System.Linq;
using Windows.UI.Xaml.Data;
using MetroLog;
using MetroLog.Targets;

namespace SendRecommendation
{
	/// Documentation on APIs used in this page:
	/// https://github.com/Windows-XAML/Template10/wiki

	[Bindable]
	sealed partial class App : Template10.Common.BootStrapper
	{
		public App()
		{
			Microsoft.ApplicationInsights.WindowsAppInitializer.InitializeAsync(
				Microsoft.ApplicationInsights.WindowsCollectors.Metadata |
				Microsoft.ApplicationInsights.WindowsCollectors.Session);
			InitializeComponent();
			SplashFactory = (e) => new Views.Splash(e);

			SetupLogging();

			#region App settings

			var _settings = SettingsService.Instance;
			RequestedTheme = _settings.AppTheme;
			CacheMaxDuration = _settings.CacheMaxDuration;
			ShowShellBackButton = _settings.UseShellBackButton;

			#endregion
		}

		private void SetupLogging()
		{
			LoggingConfiguration configuration = new LoggingConfiguration();
#if DEBUG
			configuration.AddTarget(LogLevel.Trace, LogLevel.Fatal, new DebugTarget());
			configuration.AddTarget(LogLevel.Trace, LogLevel.Fatal, new StreamingFileTarget());
#else
			configuration.AddTarget(LogLevel.Info, LogLevel.Fatal, new StreamingFileTarget());
#endif
			configuration.IsEnabled = true;

			LogManagerFactory.DefaultConfiguration = configuration;

			// setup the global crash handler...
			GlobalCrashHandler.Configure();
		}

		public override async Task OnInitializeAsync(IActivatedEventArgs args)
		{
			await Task.CompletedTask;
		}

		public override async Task OnStartAsync(StartKind startKind, IActivatedEventArgs args)
		{
			// long-running startup tasks go here

			NavigationService.Navigate(typeof(Views.MainPage));
			await Task.CompletedTask;
		}
	}
}

