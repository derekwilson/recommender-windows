﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SendRecommendation.Controls
{
	public sealed partial class ComboBoxWithTextBox : UserControl
	{
		public ComboBoxWithTextBox()
		{
			this.InitializeComponent();
		}

		public object Header
		{
			set { textBox.Header = value as String; }
		}

		public object ComboBoxTextItems
		{
			set { comboBox.SetBinding(ItemsControl.ItemsSourceProperty, value as Binding); }
		}

		public object SelectedComboBoxTextItem
		{
			set { textBox.SetBinding(TextBox.TextProperty, value as Binding); }
		}

		private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			textBox.Text = comboBox.SelectedItem as string;
		}
	}
}
