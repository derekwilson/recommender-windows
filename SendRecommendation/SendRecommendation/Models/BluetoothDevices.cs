﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;

namespace SendRecommendation.Models
{
	public class BluetoothDevices
	{
		public enum BluetoothDeviceError
		{
			OK,
			RadioOff,
			NoDevices
		}

		public BluetoothDeviceError Error { get; set; }

		public IList<DeviceInformation> Devices { get; set; }
	}
}
