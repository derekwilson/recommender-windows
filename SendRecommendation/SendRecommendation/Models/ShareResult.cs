﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendRecommendation.Models
{
	public class ShareResult
	{
		public bool Error { get; set; }

		public string ErrorMessage { get; set; }
	}
}
