﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SendRecommendation.Models
{
	public class Recommendation
	{
		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "category")]
		public string Category { get; set; }

		[JsonProperty(PropertyName = "uri")]
		public string Uri { get; set; }

		[JsonProperty(PropertyName = "notes")]
		public string Notes { get; set; }
	}
}
