﻿using Newtonsoft.Json;
using SendRecommendation.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace SendRecommendation.Services.FileServices
{
	public class JsonService
	{
		public string GetRecommendationsAsString(IList<Recommendation> recommendations)
		{
			return JsonConvert.SerializeObject(recommendations);
		}

		public IInputStream GetRecommendationsAsInputStream(IList<Recommendation> recommendations)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(GetRecommendationsAsString(recommendations));
			MemoryStream stream = new MemoryStream(bytes);
			return stream.AsRandomAccessStream();
		}

		public async Task<IRandomAccessStreamReference> GetRecommendationsAsJsonFile(IList<Recommendation> recommendations)
		{
			string jsonContents = GetRecommendationsAsString(recommendations);
			StorageFolder Folder = ApplicationData.Current.LocalFolder;
			StorageFile file = await Folder.CreateFileAsync("recommendations_share.recommendation", Windows.Storage.CreationCollisionOption.ReplaceExisting);
			IRandomAccessStream textStream = await file.OpenAsync(FileAccessMode.ReadWrite);
			{
				using (DataWriter textWriter = new DataWriter(textStream))
				{
					textWriter.WriteString(jsonContents);
					await textWriter.StoreAsync();
				}
			}
			return RandomAccessStreamReference.CreateFromFile(file);
		}
	}
}
