﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;

namespace SendRecommendation.Services.SharingServices
{
	public class ClipboardService
	{
		public async Task<string> GetStringFromClipboard()
		{
			DataPackageView dataPackageView = Clipboard.GetContent();
			if (dataPackageView.Contains("Text"))
			{
				return await dataPackageView.GetTextAsync();
			}
			return null;
		}
	}
}
