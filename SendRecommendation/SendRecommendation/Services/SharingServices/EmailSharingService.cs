﻿using MetroLog;
using Newtonsoft.Json;
using SendRecommendation.Models;
using SendRecommendation.Services.FileServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Email;
using Windows.Storage;
using Windows.Storage.Streams;

namespace SendRecommendation.Services.SharingServices
{
	 public class EmailSharingService
	{
		private ILogger _logger;

		public EmailSharingService()
		{
			_logger = LogManagerFactory.DefaultLogManager.GetLogger<EmailSharingService>();
		}

		private async Task<ShareResult> SendEmail(string address, string title, string body, IRandomAccessStreamReference attachmentData)
		{
			ShareResult result = new ShareResult();
			result.Error = false;
			try
			{
				EmailMessage message = new EmailMessage();
				message.To.Add(new EmailRecipient() { Address = address });
				message.Subject = title;
				message.Body = body;
				message.Attachments.Add(new EmailAttachment() { FileName = "recommendation_share.recommendation", Data = attachmentData });

				await EmailManager.ShowComposeNewEmailAsync(message);
			}
			catch (Exception ex)
			{
				result.Error = true;
				result.ErrorMessage = ex.Message;
			}
			return result;
		}
		public async Task<IAsyncResult> Share(string emailAddress, Recommendation recommendation, Action<ShareResult> gotoDetailsPage)
		{
			List<Recommendation> list = new List<Recommendation>(1);
			list.Add(recommendation);
			return await Share(emailAddress, list, gotoDetailsPage);
		}

		public async Task<IAsyncResult> Share(string emailAddress, IList<Recommendation> recommendations, Action<ShareResult> gotoDetailsPage)
		{
			JsonService service = new JsonService();
			IRandomAccessStreamReference attachmentData = await service.GetRecommendationsAsJsonFile(recommendations);
			_logger.Trace("Sending attachement via email to {0}.", emailAddress);
			ShareResult result = await SendEmail(emailAddress, GetSubject(recommendations), GetBody(recommendations), attachmentData);

			gotoDetailsPage(result);

			return null;
		}

		private string GetSubject(IList<Recommendation> recommendations)
		{
			if (recommendations.Count > 1)
			{
				return string.Format("I Recommend - {0} Recommendations", recommendations.Count);
			}
			else if (recommendations.Count == 1)
			{
				return string.Format("I Recommend - {0}", recommendations[0].Name);
			}
			return "I Recommend";
		}

		protected string GetNewline()
		{
			return "\n";
		}

		public string GetBody(IList<Recommendation> recommendations)
		{
			StringBuilder text = new StringBuilder();
			text.Append("I Recommend").Append(GetNewline());
			foreach (Recommendation recommendation in recommendations)
			{
				text.Append(GetNewline())
					.Append(recommendation.Name).Append(GetNewline())
					.Append(recommendation.Uri).Append(GetNewline())
					.Append(recommendation.Notes).Append(GetNewline())
					.Append("------").Append(GetNewline())
					;
			}

			text.Append(GetNewline())
					.Append(GetViralityText())
					.Append(GetNewline());
			return text.ToString();
		}

		public string GetViralityText()
		{
			StringBuilder text = new StringBuilder();
			text.Append("If you want to keep recommendations organised on your phone then try out the app, its free. ").Append(GetNewline())
				.Append("https://play.google.com/store/apps/details?id=net.derekwilson.recommender").Append(GetNewline());
			return text.ToString();
		}
	}
}
