﻿using MetroLog;
using SendRecommendation.Models;
using SendRecommendation.Services.FileServices;
using SendRecommendation.Services.SharingServices.Bluetooth;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Enumeration;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace SendRecommendation.Services.SharingServices
{
	public class BluetoothSharingService
	{
		private ILogger _logger;

		private readonly string _ftpUUID = "{00001106-0000-1000-8000-00805f9b34fb}";
		private readonly string _oppUUID = "{00001105-0000-1000-8000-00805f9b34fb}";
		private readonly string _objectName = "recommendation_share.txt";
		//private readonly string _objectType = "application/json";
		//private readonly string _objectType = "text";
		private BluetoothProfile _profile = BluetoothProfile.OBEXOPP;

		private Action<BluetoothFileTransferProgress> _progress = null;
		private BluetoothDevices _bluetoothDevices;

		public BluetoothSharingService()
		{
			_logger = LogManagerFactory.DefaultLogManager.GetLogger<EmailSharingService>();
		}

		public async Task<BluetoothDevices> Init()
		{
			_bluetoothDevices = new BluetoothDevices() { Error = BluetoothDevices.BluetoothDeviceError.OK };
			_bluetoothDevices.Devices = new List<DeviceInformation>(10);
			try
			{
				// look for paired devices and update our listbox
				var id = RfcommDeviceService.GetDeviceSelector(RfcommServiceId.ObexObjectPush);
				DeviceInformationCollection devices = await DeviceInformation.FindAllAsync(id);

				for (int i = 0; i < devices.Count; ++i)
				{
					_logger.Trace("Adding bluetooth device {0}.", devices[i].Name);
					_bluetoothDevices.Devices.Add(devices[i]);
				}
				if (devices.Count < 1)
				{
					_bluetoothDevices.Error = BluetoothDevices.BluetoothDeviceError.NoDevices;
				}
			}
			catch (Exception ex)
			{
				// suggested by MS sample, handles BT radio off case
				if ((uint)ex.HResult == 0x8007048F)
				{
					_bluetoothDevices.Error = BluetoothDevices.BluetoothDeviceError.RadioOff;
				}
			}

			return _bluetoothDevices;
		}

		public async Task<IAsyncResult> Share(DeviceInformation device, Recommendation recommendation, Action<ShareResult> gotoDetailsPage)
		{
			List<Recommendation> list = new List<Recommendation>(1);
			list.Add(recommendation);
			return await Share(device, list, gotoDetailsPage);
		}

		public async Task<IAsyncResult> Share(DeviceInformation device, IList<Recommendation> recommendations, Action<ShareResult> gotoDetailsPage)
		{
			ShareResult result = await SendRecommendations(device, recommendations);
			gotoDetailsPage(result);

			return null;
		}

		private async Task<ShareResult> SendRecommendations(DeviceInformation device, IList<Recommendation> recommendations)
		{
			_logger.Trace("Sending attachement via bluetooth to {0}.", device.Name);

			ShareResult result = new ShareResult();
			result.Error = false;

			StreamSocket socket = null;
			DataReader dataReader = null;
			DataWriter dataWriter = null;

			try
			{
				JsonService jsonservice = new JsonService();
				IInputStream data = jsonservice.GetRecommendationsAsInputStream(recommendations);

				// Initialize the target Bluetooth BR device
				RfcommDeviceService service = await RfcommDeviceService.FromIdAsync(device.Id);

				// Create a socket and connect to the target
				socket = new StreamSocket();
				await socket.ConnectAsync(
							service.ConnectionHostName,
							service.ConnectionServiceName,
							SocketProtectionLevel.BluetoothEncryptionAllowNullAuthentication);

				SetProgress(0, BluetoothFileTransferState.Connecting);

				dataReader = new DataReader(socket.InputStream);
				dataWriter = new DataWriter(socket.OutputStream);

				//send client request
				byte[] theConnectPacket = ProfilePacketFactory.CreateConnectPacket(_profile);

				dataWriter.WriteBytes(theConnectPacket);
				await dataWriter.StoreAsync();

				// Get response code
				await dataReader.LoadAsync(1);
				byte[] buffer = new byte[1];
				dataReader.ReadBytes(buffer);

				int connectionId = 0;
				if (buffer[0] == 0xA0) // Success
				{
					// Get length
					await dataReader.LoadAsync(2);
					buffer = new byte[2];
					dataReader.ReadBytes(buffer);

					int length = buffer[0] << 8;
					length += buffer[1];

					// Get rest of packet
					await dataReader.LoadAsync((uint)length - 3);
					buffer = new byte[length - 3];
					dataReader.ReadBytes(buffer);

					int obexVersion = buffer[0];
					int flags = buffer[1];
					int maxServerPacket = buffer[2] << 8 + buffer[3];

					// read FTP specific response
					if (_profile == BluetoothProfile.OBEXFTP)
					{
						int connectionIdHeader = buffer[4];
						connectionId = (buffer[5] << 24) | (buffer[6] << 16) | (buffer[7] << 8) | buffer[8];

						int whoHeader = buffer[9];
						int whoHeaderLength = (buffer[10] << 8) | buffer[11];

						byte[] whoHeaderValue = new byte[whoHeaderLength];

						Array.Copy(buffer, 12, whoHeaderValue, 0, whoHeaderLength - 3);
					}

					await PushData(_profile, _objectName, null, data.AsStreamForRead(), dataReader, dataWriter, connectionId);
				}
			}
			catch (Exception ex)
			{
				SetProgress(0, BluetoothFileTransferState.Aborted);
				result.Error = true;
				result.ErrorMessage = ex.Message;
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Dispose();
				}
				if (dataWriter != null)
				{
					dataWriter.Dispose();
				}
				if (socket != null)
				{
					socket.Dispose();
				}
			}
			return result;
		}

		private async Task<bool> PushData(BluetoothProfile profile, string objectName, string objectType, Stream stream, DataReader dataReader, DataWriter dataWriter, int connectionId)
		{
			int blockSize = ProfilePacketFactory.DataPacketSize;

			// Chop data into packets           
			int blocks = (int)Math.Ceiling((float)stream.Length / blockSize);

			byte[] packet;
			bool result = false;

			// it seems that android devices very often respond with 0xcb, after the second packet is sent.
			// According to specification it means "Length header required", but we apparently do send it in our first packet.
			// The workaround found is to ignore this response and continue the transfer, somehow it works.
			bool androidMode = false;

			int i = 0;

			for (i = 0; i < blocks; ++i)
			{
				packet = ProfilePacketFactory.CreateDataPacket(profile, objectName, objectType, stream, connectionId, blockSize);

				dataWriter.WriteBytes(packet);
				await dataWriter.StoreAsync();

				// Get response code
				await dataReader.LoadAsync(3);
				byte[] buffer = new byte[3];
				dataReader.ReadBytes(buffer);

				int responseLength = (buffer[1] << 8 & 0xFF00) | (buffer[2] & 0xFF);
				if (responseLength > 3 && responseLength == dataReader.UnconsumedBufferLength)
				{
					byte[] response = new byte[responseLength - 3];
					dataReader.ReadBytes(response);
				}

				if (!androidMode)
				{
					// check the response to be valid
					if (buffer[0] != 0xA0 && buffer[0] != 0x90 && buffer[0] != 0xcb)
					{
						throw new InvalidOperationException(string.Format("Bad OBEX response code 0X{0:X}", buffer[0]));
					}
					else if (buffer[0] == 0xA0) // Success
					{
						result = true;
						break;
					}
					else if (buffer[0] == 0xcb)
					{
						androidMode = true;
					}
				}

				SetProgress(((float)i / blocks) * 100, BluetoothFileTransferState.InProgress);
			}

			//so we sent all blocks and there was an andoid mode on
			if (i == blocks && androidMode)
			{
				result = true;
			}

			// attemp to issue a disconnect packet
			byte[] bytes = new byte[3];
			bytes[0] = 0x81;
			bytes[1] = 0;
			bytes[2] = 3;

			if (dataWriter != null)
			{
				dataWriter.WriteBytes(bytes);
				await dataWriter.StoreAsync();
			}

			if (result)
			{
				SetProgress(100, BluetoothFileTransferState.Completed);
			}

			return result;
		}

		/// <summary>
		/// Gets the profile UUID for corresponding <see cref="BluetoothProfile"/> object.
		/// </summary>
		/// <param name="profile">The profile.</param>
		/// <returns>String representation of the UUID</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">profile</exception>
		private string GetProfileUUID(BluetoothProfile profile)
		{
			switch (profile)
			{
				case BluetoothProfile.OBEXOPP:
					return _oppUUID;
				case BluetoothProfile.OBEXFTP:
					return _ftpUUID;
				default:
					throw new ArgumentOutOfRangeException("profile");
			}
		}

		/// <summary>
		/// Sets the progress of the operation if the callback has been set.
		/// </summary>
		/// <param name="percentage">The percentage.</param>
		/// <param name="state">The state.</param>
		private void SetProgress(float percentage, BluetoothFileTransferState state)
		{
			if (_progress != null)
			{
				_progress(new BluetoothFileTransferProgress(percentage, state));
			}
		}
	}
}
