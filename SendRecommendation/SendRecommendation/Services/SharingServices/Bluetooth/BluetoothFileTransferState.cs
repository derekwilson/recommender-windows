﻿/* Copyright by Apitron LTD 2014,
 The code is free to use in any project.
 Author Robert McKalkin.*/

namespace SendRecommendation.Services.SharingServices.Bluetooth
{
	/// <summary>
	/// Defines possible transfer states.
	/// </summary>
	enum BluetoothFileTransferState
    {
        Connecting,
        InProgress,
        Completed,
        Aborted
    }
}
